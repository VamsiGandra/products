## Getting started

Start up a live server(VS Code extension) and run `src\refactor-me\index.html`

## How to run tests

- Ensure Node.js & npm are installed
- `npm install -g karma-cli` (to install the karma-cli package globally)
- `npm install` (to install the dependencies)
- `karma start` (to start karma and run tests)
