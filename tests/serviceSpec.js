describe('service tests', function () {
  var productServiceObj;

  beforeEach(function () {
    module(function ($provide) {
    });

    module('app.core');
  });

  beforeEach(inject(function (ProductService) {
    productServiceObj = ProductService;
  }));


  it('should transform the product with type and default exchage rate given', function () {
    var products = [
      {
        id: 1,
        name: "Amazon Fire Burgundy Phone Case",
        colour: "Burgundy",
        material: "PVC",
        targetPhone: "Amazon Fire",
        price: 14.0
      }, {
        id: 2,
        name: "Nokia Lumia 920/930/Icon Crimson Phone Case",
        colour: "Red",
        material: "Rubber",
        targetPhone: "Nokia Lumia 920/930/Icon",
        price: 10.0
      }
    ];

    var productType = 'Phone Case';

    var expectedProductsTransformed = [{ id: 1, name: 'Amazon Fire Burgundy Phone Case', type: 'Phone Case', price: '14.00' }, { id: 2, name: 'Nokia Lumia 920/930/Icon Crimson Phone Case', type: 'Phone Case', price: '10.00' }]

    expect(products.map(productServiceObj.processItem(productType))).toEqual(expectedProductsTransformed);
  });


  it('should transform the product with type and price with the exchage rate given', function () {
    var products = [
      {
        id: 1,
        name: "Amazon Fire Burgundy Phone Case",
        colour: "Burgundy",
        material: "PVC",
        targetPhone: "Amazon Fire",
        price: 14.0
      }, {
        id: 2,
        name: "Nokia Lumia 920/930/Icon Crimson Phone Case",
        colour: "Red",
        material: "Rubber",
        targetPhone: "Nokia Lumia 920/930/Icon",
        price: 10.0
      }
    ];

    var productType = 'Phone Case';
    var USDExchangeRate = 0.76;

    var expectedProductsTransformed = [{id: 1, name: 'Amazon Fire Burgundy Phone Case', type: 'Phone Case', price: '10.64'}, {id: 2, name: 'Nokia Lumia 920/930/Icon Crimson Phone Case', type: 'Phone Case', price: '7.60'}];


    expect(products.map(productServiceObj.processItem(productType, USDExchangeRate))).toEqual(expectedProductsTransformed);
  });


  it('should return proper product types', function () {

    expect(productServiceObj.lawnMowerType).toBe('Lawnmower');
    expect(productServiceObj.phoneCaseType).toBe('Phone Case');
    expect(productServiceObj.tShirtType).toBe('T-Shirt');
  });

});
