angular.module('app.core', []).service('ProductService', function () {

    this.lawnMowerType  = 'Lawnmower';
    this.phoneCaseType  = 'Phone Case';
    this.tShirtType     = 'T-Shirt';

    this.getLawnmowerProducts = function () {
        return new LawnmowerRepository().getAll();
    }

    this.getPhoneCaseProducts = function () {
        return new PhoneCaseRepository().getAll();
    }

    this.getTShirtProducts = function () {
        return new TShirtRepository().getAll();
    }

    this.processItem = (type, exchangeRate = 1) => ({
        id,
        price,
        name,
    }) => ({
        id,
        name,
        type,
        price: (price * exchangeRate).toFixed(2),
    });

    this.getAllProductsByCurrency = function (currency, exchangeRate = 1) {

        return [{
            products:
                [
                    ...this.getLawnmowerProducts().map(this.processItem(this.lawnMowerType, exchangeRate)),
                    ...this.getPhoneCaseProducts().map(this.processItem(this.phoneCaseType, exchangeRate)),
                    ...this.getTShirtProducts().map(this.processItem(this.tShirtType, exchangeRate))
                ],
            currency
        }]


    }

});