ProductsTableController.$inject = ['$scope', 'ProductService'];
function ProductsTableController($scope, ProductService) {
}


angular.module('app.core').component('productsTable', {

    controller: ProductsTableController,
    controllerAs: 'ProductsTable',
    bindings: {
        products: '<',
        currency: '<'
    },
    templateUrl: 'components/products/productsTable.html',
});