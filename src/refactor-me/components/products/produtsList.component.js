ProductListController.$inject = ['$scope', 'ProductService'];
function ProductListController($scope, ProductService) {

    this.NZDCurrency = 'NZD';
    this.USDCurrency = 'USD';
    this.EuroCurrency = 'Euro';
    
    this.USDExchangeRate = 0.76;
    this.EurosExchangeRate = 0.67;

    this.productsInNZD = ProductService.getAllProductsByCurrency(this.NZDCurrency);
    this.productsInUSDollars = ProductService.getAllProductsByCurrency(this.USDCurrency, this.USDExchangeRate);
    this.productsInEuros = ProductService.getAllProductsByCurrency(this.EuroCurrency, this.EurosExchangeRate);

    this.productsConsolidated = [...this.productsInNZD, ...this.productsInUSDollars, ...this.productsInEuros];
}

angular.module('app.core').component('productList', {

    controller: ProductListController,
    controllerAs: 'ProductList',
    bindings: {
        products: '<',
        currency: '<'
    },
    templateUrl: 'components/products/productsList.html',
});